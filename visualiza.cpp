#include <GL/glut.h>
#include <math.h>

GLfloat v1[4][3] = { { 0,0,0.2 },{ 0,1,0.2 },{ 1,1,0.2 },{ 1,0,0.2 } };

GLfloat v2[3][3] = { { 0.2,-0.1,0.6 },{ 0.6,1.2,-0.8 },{ 0.8,-0.1,0.6 } };


void quadrado() {

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);

	glVertex3fv(v1[0]); //frente
	glVertex3fv(v1[1]);
	glVertex3fv(v1[2]);
	glVertex3fv(v1[3]);

	glEnd();
}

void triangulo() {

	GLubyte indices2[3] = { 0, 1, 2 };
	glBegin(GL_TRIANGLES);

	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3fv(v2[0]); //frente
	glVertex3fv(v2[1]);
	glVertex3fv(v2[2]);

	glEnd();
}

//Fun??o callback chamada para fazer o desenho
void Desenha(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Limpa a janela de visualiza??o com a cor de fundo especificada
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	triangulo();
	quadrado();


	// Executa os comandos OpenGL

	glutSwapBuffers();
	glFinish();
}

// Inicializa par?metros de rendering
void Inicializa(void)
{
	// Define a cor de fundo da janela de visualiza??o como preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

}

// Fun??o callback chamada quando o tamanho da janela ? alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Evita a divisao por zero
	if (h == 0) h = 1;

	// Especifica as dimens?es da Viewport
	glViewport(0, 0, w, h);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele??o (left, right, bottom, top)
	if (w <= h)
		gluOrtho2D(-1.0f, 2.0f, -1.0f, 2.0f*h / w);
	else
		gluOrtho2D(-1.0f, 2.0f*w / h, -1.0f, 2.0f);
}

// Programa Principal
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(400, 350);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Visualizacao");
	glutDisplayFunc(Desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	Inicializa();
	glutMainLoop();
}
