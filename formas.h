#include <math.h>
#include <GL/glut.h>

void cubo(GLfloat center[3], GLfloat size);

// Função Esfera: aproximação usando quadrilateros
// E as formulas:
// x(theta,phi) = r seno(phi) coseno(theta)
// y(theta,phi) = r seno(phi) seno(theta)
// z(theta,phi) = r coseno(phi)
void esfera(GLfloat centro[3], GLdouble radius, GLfloat num);
