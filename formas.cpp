#include "formas.h"

void cubo(GLfloat center[3], GLfloat size) {
    glBegin(GL_QUADS);

    GLfloat vertexs[8][3] = {
        { center[0] - size, center[1] - size, center[2] + size },  // A - 0
        { center[0] + size, center[1] - size, center[2] + size },  // B - 1
        { center[0] + size, center[1] + size, center[2] + size },  // C - 2
        { center[0] - size, center[1] + size, center[2] + size },  // D - 3
        { center[0] - size, center[1] + size, center[2] - size },  // E - 4
        { center[0] + size, center[1] + size, center[2] - size },  // F - 5
        { center[0] + size, center[1] - size, center[2] - size },  // G - 6
        { center[0] - size, center[1] - size, center[2] - size },  // H - 7
    };

    // Back - Red
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
    glVertex3fv(vertexs[0]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[3]);

    // Left - Cyan
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[0]);

    // Top - Blue
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[2]);

    // Bottom - Pink
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[0]);

    // Right - Green
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[6]);

    // Front - Yellow
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[4]);

    glEnd();
}

void esfera(GLdouble centro[3], GLdouble radius, GLfloat num) {
	GLdouble c, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
	GLdouble phi1, theta1, phi2, theta2;
	GLdouble senphi1, cosphi1, senphi2, cosphi2,
	         sentheta1, costheta1, sentheta2, costheta2,
	         passo;

	glVertex3d(centro[0], centro[1] + radius, centro[2]);

	c = M_PI / 180;
	passo = 180 / num * c;
	glBegin(GL_TRIANGLES);
	phi1 = 0;
	senphi1 = sin(phi1);
	cosphi1 = cos(phi1);
	int i,j;
	for (i=0; i < num; i++) {
		phi2 = phi1 + passo;
		senphi2 = sin(phi2);
		cosphi2 = cos(phi2);
		theta1 = 0;
		sentheta1 = sin(theta1);
		costheta1 = cos(theta1);
		for (j=0; j < 2 * num; j++) {
			theta2 = theta1 + passo;
			sentheta2 = sin(theta2);
			costheta2 = cos(theta2);
			x1 = centro[0] + radius * senphi1 * costheta1;
			y1 = centro[1] + radius * senphi1 * sentheta1;
			z1 = centro[2] + radius * cosphi1;
			glVertex3d(x1, y1, z1);
			x2 = centro[0] + radius * senphi2 * costheta1;
			y2 = centro[1] + radius * senphi2 * sentheta1;
			z2 = centro[2] + radius * cosphi2;
			glVertex3d(x2, y2, z2);
			x3 = centro[0] + radius * senphi2 * costheta2;
			y3 = centro[1] + radius * senphi2 * sentheta2;
			z3 = centro[2] + radius * cosphi2;
			glVertex3d(x3, y3, z3);
			glVertex3d(x1, y1, z1);
			glVertex3d(x3, y3, z3);
			x4 = centro[0] + radius * senphi1 * costheta2;
			y4 = centro[1] + radius * senphi1 * sentheta2;
			z4 = centro[2] + radius * cosphi1;
			glVertex3d(x4, y4, z4);
			theta1 = theta2;
			sentheta1 = sentheta2;
			costheta1 = costheta2;
		}
		phi1 = phi2;
		senphi1 = senphi2;
		cosphi1 = cosphi2;
	}
}

