#include <GL/glut.h>

GLfloat p[3][2] = { {0.0, 0.0},
				    {50.0, 100.0},
				    {100.0, 0.0} };

GLint n;

// Fun��o Triangulo: Emite os vertices do triagulo
void triangulo(GLfloat *v1, GLfloat *v2, GLfloat *v3) {
	glVertex2fv(v1);
	glVertex2fv(v2);
	glVertex2fv(v3);
}
// Fun��o divTriangulo: fun��o que divide um triagulo
void divTriangulo(GLfloat *v1, GLfloat *v2, GLfloat *v3, int k) {

	GLfloat v12[2], v13[2], v23[2];

	int i;

	if (k == 0) {
		triangulo(v1, v2, v3);
		return;
	}

	v12[0] = (v1[0] + v2[0]) / 2;
	v12[1] = (v1[1] + v2[1]) / 2;
	v13[0] = (v1[0] + v3[0]) / 2;
	v13[1] = (v1[1] + v3[1]) / 2;
	v23[0] = (v2[0] + v3[0]) / 2;
	v23[1] = (v2[1] + v3[1]) / 2;

	divTriangulo(v1, v12, v13, k - 1);
	divTriangulo(v2, v12, v23, k - 1);
	divTriangulo(v3, v13, v23, k - 1);
}

// Fun��o callback chamada para fazer o desenho
void Desenha(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Limpa a janela de visualiza��o com a cor de fundo especificada
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_TRIANGLES);
	divTriangulo(p[0], p[1], p[2], n);
	glEnd();

	// Executa os comandos OpenGL
	glFlush();
}

// Inicializa par�metros de rendering
void Inicializa(void)
{
	// Define a cor de fundo da janela de visualiza��o como preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	n = 5;

}

// Fun��o callback chamada quando o tamanho da janela � alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Evita a divisao por zero
	if (h == 0) h = 1;

	// Especifica as dimens�es da Viewport
	glViewport(100, 50, w, h);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

//	Estabelece a janela de sele��o (left, right, bottom, top)
	if (w <= h)
		gluOrtho2D(0.0f, 250.0f, 0.0f, 250.0f*h / w);
	else
		gluOrtho2D(0.0f, 250.0f*w / h, 0.0f, 250.0f);

}

// Programa Principal
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 350);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Gasket");
	glutDisplayFunc(Desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	Inicializa();
	glutMainLoop();
}
