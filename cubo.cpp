#include <stdio.h>
#include <GL/glut.h>

#define VERT_A vertexs[0]
#define VERT_B vertexs[1]
#define VERT_C vertexs[2]
#define VERT_D vertexs[3]
#define VERT_E vertexs[4]
#define VERT_F vertexs[5]
#define VERT_G vertexs[6]
#define VERT_H vertexs[7]

static const GLfloat vertexs[8][3] = {
    { -1.0, -1.0,  1.0 },  // A - 0
    {  1.0, -1.0,  1.0 },  // B - 1
    {  1.0,  1.0,  1.0 },  // C - 2
    { -1.0,  1.0,  1.0 },  // D - 3
    { -1.0,  1.0, -1.0 },  // E - 4
    {  1.0,  1.0, -1.0 },  // F - 5
    {  1.0, -1.0, -1.0 },  // G - 6
    { -1.0, -1.0, -1.0 }   // H - 7
};

static GLfloat rotation[2][4] = {
    { 0.0f, 1.0f, 0.0f, 0.0f }, // X
    { 0.0f, 0.0f, 1.0f, 0.0f }  // Y
};

struct rotctl {
    bool rotating = false;
    GLfloat rotation_increment = 1.0f;
    unsigned char axis = 0; // 0 = X, 1 = Y, 2 = Z
} rotation_control;

// Função callback chamada para fazer o desenho
void Desenha(void)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // rotate on x, y
    glRotatef(rotation[0][0], rotation[0][1], rotation[0][2], rotation[0][3]);
    glRotatef(rotation[1][0], rotation[1][1], rotation[1][2], rotation[1][3]);

    // Limpa a janela de visualização com a cor de fundo especificada
    glClear(GL_COLOR_BUFFER_BIT);

    // Back - Red
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
    glVertex3fv(vertexs[0]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[3]);

    // Left - Cyan
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[0]);

    // Top - Blue
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[2]);

    // Bottom - Pink
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[0]);

    // Right - Green
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[6]);

    // Front - Yellow
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[4]);

    // Para de desenhar
    glEnd();

    // Executa os comandos OpenGL
    glFlush();
}

// Inicializa parâmetros de rendering
void Inicializa(void)
{
    // Define a cor de fundo da janela de visualização como preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void rotationInput(int key, int x, int y) {
    switch (key) {
    case GLUT_KEY_UP:
        rotation[0][0] -= 5.0f;
        break;
    case GLUT_KEY_DOWN:
        rotation[0][0] += 5.0f;
        break;
    case GLUT_KEY_LEFT:
        rotation[1][0] -= 5.0f;
        break;
    case GLUT_KEY_RIGHT:
        rotation[1][0] += 5.0f;
        break;
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

void keyboardInput(unsigned char key, int x, int y) {
    switch (key) {
    case 'r':
    case 'R':
        printf("Started Rotation!\n");
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

// Função callback chamada quando o tamanho da janela é alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
    // Evita a divisao por zero
    if (h == 0) h = 1;

    // Especifica as dimens�es da Viewport
    glViewport(0, 0, w, h);

    // Inicializa o sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Estabelece a janela de sele��o (left, right, bottom, top)
    // if (w <= h)
    // 	gluOrtho2D(-2.0f, 2.0f, -2.0f, 2.0f*h / w);
    // else

    // Define "campo de visão"
    glOrtho(-2.0f, 2.0f, -2.0f, 2.0f, 2.0f, -2.0f);
}

// Programa Principal
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 350);
    glutInitWindowPosition(10, 10);
    glutCreateWindow("Cubo");
    glutDisplayFunc(Desenha);
    glutSpecialFunc(rotationInput);
    glutReshapeFunc(AlteraTamanhoJanela);
    Inicializa();
    glutMainLoop();
}
