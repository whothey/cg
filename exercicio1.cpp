#include <GL/glut.h>
#include <math.h>

GLfloat v[8][3] = { {-1,-1,1}, {1,-1,1}, {1,1,1}, {-1,1,1},
					{-1,-1,-1}, {1,-1,-1}, {1,1,-1}, {-1,1,-1} };

void cubo() {
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3fv(v[7]); // fundos
	glVertex3fv(v[6]);
	glVertex3fv(v[5]);
	glVertex3fv(v[4]);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3fv(v[0]); // base
	glVertex3fv(v[1]);
	glVertex3fv(v[5]);
	glVertex3fv(v[4]);

  glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3fv(v[1]); // dir
	glVertex3fv(v[5]);
	glVertex3fv(v[6]);
	glVertex3fv(v[2]);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3fv(v[0]); // esq
	glVertex3fv(v[3]);
	glVertex3fv(v[7]);
	glVertex3fv(v[4]);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3fv(v[3]); // topo
	glVertex3fv(v[2]);
	glVertex3fv(v[6]);
	glVertex3fv(v[7]);


	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3fv(v[0]); //frente
	glVertex3fv(v[1]);
	glVertex3fv(v[2]);
	glVertex3fv(v[3]);

glEnd();
}

//Fun??o callback chamada para fazer o desenho
void Desenha(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Limpa a janela de visualiza??o com a cor de fundo especificada
	glClear(GL_COLOR_BUFFER_BIT);

	glRotatef(45, 1,0,0);
	glRotatef(45, 0,1,0);

	cubo();
	// Executa os comandos OpenGL
	glFlush();
}

// Inicializa par?metros de rendering
void Inicializa(void)
{
	// Define a cor de fundo da janela de visualiza??o como preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_SMOOTH);

}

// Fun??o callback chamada quando o tamanho da janela ? alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Evita a divisao por zero
	if (h == 0) h = 1;

	// Especifica as dimens?es da Viewport
	glViewport(0, 0, w, h);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele??o (left, right, bottom, top)
	glOrtho(-2.0f, 2.0f, -2.0f, 2.0f,2.0f,-2.0f);
	//gluOrtho2D(-2.0f, 2.0f, -2.0f, 2.0f);
}

// Programa Principal
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 350);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Cubo");
	glutDisplayFunc(Desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	Inicializa();
	glutMainLoop();
}
