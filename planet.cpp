#include <stdio.h>
#include <GL/glut.h>
#include "formas.h"

static GLfloat rotation[2][4] = {
    { 0.0f, 1.0f, 0.0f, 0.0f }, // X
    { 0.0f, 0.0f, 1.0f, 0.0f }  // Y
};

struct rotctl {
    bool rotating = false;
    GLfloat rotation_increment = 1.0f;
    unsigned char axis = 0; // 0 = X, 1 = Y, 2 = Z
} rotation_control;

GLfloat diskrot = 0.0f;
GLfloat planetrot = 0.0f;

// Função callback chamada para fazer o desenho
void Desenha(void)
{
    GLUquadricObj *quad = gluNewQuadric();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);

    // rotate on x, y
    glRotatef(rotation[0][0], rotation[0][1], rotation[0][2], rotation[0][3]);
    glRotatef(rotation[1][0], rotation[1][1], rotation[1][2], rotation[1][3]);

    // Limpa a janela de visualização com a cor de fundo especificada
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gluQuadricDrawStyle(quad, GLU_FILL);

    glColor3f(1, 0, 0);
    glRotatef(planetrot += 0.005, 1, 0, 0);
    gluSphere(quad, 1, 100, 100);

    gluQuadricDrawStyle(quad, GLU_SILHOUETTE);

    glColor3f(0, 0, 1);
    glRotatef(diskrot += 0.005, 0, 1, 1);
    gluDisk(quad, 1.5, 2.5, 100, 100);

    // Executa os comandos OpenGL
    glFlush();

    // Redraw!
    glutPostRedisplay();
}

// Inicializa parâmetros de rendering
void Inicializa(void)
{
    // Define a cor de fundo da janela de visualização como preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void rotationInput(int key, int x, int y) {
    switch (key) {
    case GLUT_KEY_UP:
        rotation[0][0] -= 5.0f;
        break;
    case GLUT_KEY_DOWN:
        rotation[0][0] += 5.0f;
        break;
    case GLUT_KEY_LEFT:
        rotation[1][0] -= 5.0f;
        break;
    case GLUT_KEY_RIGHT:
        rotation[1][0] += 5.0f;
        break;
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

void keyboardInput(unsigned char key, int x, int y) {
    switch (key) {
    case 'r':
    case 'R':
        printf("Started Rotation!\n");
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

// Função callback chamada quando o tamanho da janela é alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
    // Evita a divisao por zero
    if (h == 0) h = 1;

    // Especifica as dimens�es da Viewport
    glViewport(0, 0, w, h);

    // Inicializa o sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Estabelece a janela de sele��o (left, right, bottom, top)
    // if (w <= h)
    // 	gluOrtho2D(-2.0f, 2.0f, -2.0f, 2.0f*h / w);
    // else

    // Define "campo de visão"
    //glOrtho(-2.0f, 2.0f, -2.0f, 2.0f, 2.0f, -2.0f);
    gluPerspective(45, w/h, 2, 100);
}

// Programa Principal
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(400, 350);
    glutInitWindowPosition(10, 10);
    glutCreateWindow("Simulado CG");
    glutDisplayFunc(Desenha);
    glutSpecialFunc(rotationInput);
    glutReshapeFunc(AlteraTamanhoJanela);
    Inicializa();
    glutMainLoop();
}
