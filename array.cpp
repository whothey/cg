#include <GL/glut.h>
#include <math.h>

GLfloat v[8][3] = { {0,0,0}, {0,1,0}, {1,1,0}, {1,0,0},
					{0.5,0.5,1}, {0.5,1.5,1}, {1.5,1.5,1}, {1.5,0.5,1} };
GLuint index;

void cubo1() {
	glBegin(GL_QUADS);

	glVertex3fv(v[0]); //frente
	glVertex3fv(v[1]);
	glVertex3fv(v[2]);
	glVertex3fv(v[3]);

	glVertex3fv(v[4]); // fundos
	glVertex3fv(v[5]);
	glVertex3fv(v[6]);
	glVertex3fv(v[7]);

	glVertex3fv(v[0]); // esq
	glVertex3fv(v[1]);
	glVertex3fv(v[5]);
	glVertex3fv(v[4]);

	glVertex3fv(v[1]); // topo
	glVertex3fv(v[5]);
	glVertex3fv(v[6]);
	glVertex3fv(v[2]);

	glVertex3fv(v[3]); // dir
	glVertex3fv(v[2]);
	glVertex3fv(v[6]);
	glVertex3fv(v[7]);

	glVertex3fv(v[0]); // base
	glVertex3fv(v[4]);
	glVertex3fv(v[7]);
	glVertex3fv(v[3]);

	glEnd();
}
void cubo2() {

	GLfloat vertices[24][3] = { { 0,0,0 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 },
	{ 0.5,0.5,1 },{ 0.5,1.5,1 },{ 1.5,1.5,1 },{ 1.5,0.5,1 },
	{ 0, 0, 0 },{ 0,1,0 },{ 0.5,1.5,1 },{ 0.5,0.5,1 },
	{ 0,1,0 },{ 0.5,1.5,1 },{ 1.5,1.5,1 },{ 1,1,0 },
	{ 1,0,0 },{ 1,1,0 },{ 1.5,1.5,1 },{ 1.5,0.5,1 },
	{ 0,0,0 },{ 0.5,0.5,1 },{ 1.5,0.5,1 },{ 1,0,0 } };


	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, vertices);

	// draw a cube
	glDrawArrays(GL_QUADS, 0, 24);

	// deactivate vertex arrays after drawing
	glDisableClientState(GL_VERTEX_ARRAY);
}
void cubo3() {

	GLfloat color[8][3] = { { 0,0,0 },{ 0,0,1 },{ 0,1,0 },{ 0,1,1 }, {1,0,0}, {1,0,1}, {1,1,0}, {1,1,1} };
	GLubyte indices[24] = { 0, 1, 2, 3, 4, 5, 6, 7,
		                    0, 1, 5, 4, 1, 5, 6, 2,
		                    3, 2, 6, 7, 0, 4, 7, 3};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, v);
	glColorPointer(3, GL_FLOAT, 0, color);

	// draw a cube
	glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, indices);

	// deactivate vertex arrays after drawing
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}


void inicializaCubo4() {

	index = glGenLists(1);

	glNewList(index, GL_COMPILE);

	glBegin(GL_QUADS);

	glVertex3fv(v[0]); //frente
	glVertex3fv(v[1]);
	glVertex3fv(v[2]);
	glVertex3fv(v[3]);

	glVertex3fv(v[4]); // fundos
	glVertex3fv(v[5]);
	glVertex3fv(v[6]);
	glVertex3fv(v[7]);

	glVertex3fv(v[0]); // esq
	glVertex3fv(v[1]);
	glVertex3fv(v[5]);
	glVertex3fv(v[4]);

	glVertex3fv(v[1]); // topo
	glVertex3fv(v[5]);
	glVertex3fv(v[6]);
	glVertex3fv(v[2]);

	glVertex3fv(v[3]); // dir
	glVertex3fv(v[2]);
	glVertex3fv(v[6]);
	glVertex3fv(v[7]);

	glVertex3fv(v[0]); // base
	glVertex3fv(v[4]);
	glVertex3fv(v[7]);
	glVertex3fv(v[3]);

	glEnd();

	glEndList();
}

void cubo4() {
	glCallList(index);
}

//Fun??o callback chamada para fazer o desenho
void Desenha(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Limpa a janela de visualiza??o com a cor de fundo especificada
	glClear(GL_COLOR_BUFFER_BIT);

	cubo4();
	// Executa os comandos OpenGL
	glFlush();
}

// Inicializa par?metros de rendering
void Inicializa(void)
{
	// Define a cor de fundo da janela de visualiza??o como preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	inicializaCubo4();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_SMOOTH);

}

// Fun??o callback chamada quando o tamanho da janela ? alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Evita a divisao por zero
	if (h == 0) h = 1;

	// Especifica as dimens?es da Viewport
	glViewport(0, 0, w, h);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele??o (left, right, bottom, top)
	if (w <= h)
		gluOrtho2D(-0.5f, 2.0f, -0.5f, 2.0f*h / w);
	else
		gluOrtho2D(-0.5f, 2.0f*w / h, -0.5f, 2.0f);
}

// Programa Principal
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 350);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Cubo");
	glutDisplayFunc(Desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	Inicializa();
	glutMainLoop();
}
