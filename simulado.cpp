#include <stdio.h>
#include <GL/glut.h>
#include <math.h>

static GLfloat rotation[2][4] = {
    { 0.0f, 1.0f, 0.0f, 0.0f }, // X
    { 0.0f, 0.0f, 1.0f, 0.0f }  // Y
};

struct rotctl {
    bool rotating = false;
    GLfloat rotation_increment = 1.0f;
    unsigned char axis = 0; // 0 = X, 1 = Y, 2 = Z
} rotation_control;

GLfloat approx = 10.0f;
GLfloat sphererotx = 0.0f;
GLfloat sphereroty = 0.0f;
GLfloat cuberot = 0.0f;
GLfloat cubecenterrot = 0.0f;
static GLfloat center[3] = {0, 0, 0};

void esfera(GLfloat centro[3], GLdouble radius, GLfloat num) {
	GLdouble c, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
	GLdouble phi1, theta1, phi2, theta2;
	GLdouble senphi1, cosphi1, senphi2, cosphi2,
	         sentheta1, costheta1, sentheta2, costheta2,
	         passo;

    GLfloat colors[6][3] = {
        { 1, 0, 0 },
        { 0, 1, 0 },
        { 0, 0, 1 },
        { 1, 0, 1 },
        { 1, 1, 1 },
        { 0, 1, 1 }
    };

	glVertex3d(centro[0], centro[1] + radius, centro[2]);

	c = M_PI / 180;
	passo = 180 / num * c;
	glBegin(GL_TRIANGLES);
	phi1 = 0;
	senphi1 = sin(phi1);
	cosphi1 = cos(phi1);
	int i,j;

    glColor3fv(colors[0]);

	for (i=0; i < num; i++) {
		phi2 = phi1 + passo;
		senphi2 = sin(phi2);
		cosphi2 = cos(phi2);
		theta1 = 0;
		sentheta1 = sin(theta1);
		costheta1 = cos(theta1);

        glColor3fv(colors[i % 5]);

		for (j=0; j < 2 * num; j++) {

			theta2 = theta1 + passo;
			sentheta2 = sin(theta2);
			costheta2 = cos(theta2);
			x1 = centro[0] + radius * senphi1 * costheta1;
			y1 = centro[1] + radius * senphi1 * sentheta1;
			z1 = centro[2] + radius * cosphi1;
			glVertex3d(x1, y1, z1);
			x2 = centro[0] + radius * senphi2 * costheta1;
			y2 = centro[1] + radius * senphi2 * sentheta1;
			z2 = centro[2] + radius * cosphi2;
			glVertex3d(x2, y2, z2);
			x3 = centro[0] + radius * senphi2 * costheta2;
			y3 = centro[1] + radius * senphi2 * sentheta2;
			z3 = centro[2] + radius * cosphi2;
			glVertex3d(x3, y3, z3);
			glVertex3d(x1, y1, z1);
			glVertex3d(x3, y3, z3);
			x4 = centro[0] + radius * senphi1 * costheta2;
			y4 = centro[1] + radius * senphi1 * sentheta2;
			z4 = centro[2] + radius * cosphi1;
			glVertex3d(x4, y4, z4);
			theta1 = theta2;
			sentheta1 = sentheta2;
			costheta1 = costheta2;
		}
		phi1 = phi2;
		senphi1 = senphi2;
		cosphi1 = cosphi2;
	}
}


void cubo(GLfloat center[3], GLfloat size) {
    glBegin(GL_QUADS);

    GLfloat vertexs[8][3] = {
        { center[0] - size, center[1] - size, center[2] + size },  // A - 0
        { center[0] + size, center[1] - size, center[2] + size },  // B - 1
        { center[0] + size, center[1] + size, center[2] + size },  // C - 2
        { center[0] - size, center[1] + size, center[2] + size },  // D - 3
        { center[0] - size, center[1] + size, center[2] - size },  // E - 4
        { center[0] + size, center[1] + size, center[2] - size },  // F - 5
        { center[0] + size, center[1] - size, center[2] - size },  // G - 6
        { center[0] - size, center[1] - size, center[2] - size },  // H - 7
    };

    // Back - Red
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
    glVertex3fv(vertexs[0]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[3]);

    // Left - Cyan
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[0]);

    // Top - Blue
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[3]);
    glVertex3fv(vertexs[4]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[2]);

    // Bottom - Pink
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[0]);

    // Right - Green
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[1]);
    glVertex3fv(vertexs[2]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[6]);

    // Front - Yellow
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3fv(vertexs[7]);
    glVertex3fv(vertexs[6]);
    glVertex3fv(vertexs[5]);
    glVertex3fv(vertexs[4]);

    glEnd();
}

// Função callback chamada para fazer o desenho
void Desenha(void)
{
    GLUquadricObj *quad = gluNewQuadric();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(0, 0, approx, 0, 0, 0, 0, 1, 0);

    // rotate on x, y
    glRotatef(rotation[0][0], rotation[0][1], rotation[0][2], rotation[0][3]);
    glRotatef(rotation[1][0], rotation[1][1], rotation[1][2], rotation[1][3]);

    // Limpa a janela de visualização com a cor de fundo especificada
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gluQuadricDrawStyle(quad, GLU_FILL);

    // Sphere
    glPushMatrix();

    glRotatef(sphererotx += 0.1, 1, 0, 0);
    glRotatef(sphereroty += 0.5, 0, 1, 0);
    esfera(center, 2, 15);

    glPopMatrix();

    // Cone

    glPushMatrix();

    glLoadIdentity();

    glColor3f(.3, .3, .3);
    glRotatef(90, 1, 0, 0);
    glRotatef(sphererotx, 1, 0, 0);
    glRotatef(sphereroty, 0, 0, 1);
    glTranslatef(0, 0, 0.9);
    gluCylinder(quad, 1.8, 0, 4, 100, 100);

    glPopMatrix();

    // Cubo

    glPushMatrix();

    glColor3f(.3, .3, .3);
    glRotatef(cubecenterrot += 0.05, 0, 0, 1);
    glTranslatef(7, 0, 0);
    glRotatef(cuberot += 0.05, 1, 0, 0);
    cubo(center, 1);

    glPopMatrix();

    // Executa os comandos OpenGL
    glFlush();

    // Redraw!
    glutPostRedisplay();
}

// Inicializa parâmetros de rendering
void Inicializa(void)
{
    // Define a cor de fundo da janela de visualização como preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void rotationInput(int key, int x, int y) {
    switch (key) {
    case GLUT_KEY_UP:
        approx += 1;
        break;
    case GLUT_KEY_DOWN:
        approx -= 1;
        break;
    case GLUT_KEY_LEFT:
        rotation[1][0] -= 5.0f;
        break;
    case GLUT_KEY_RIGHT:
        rotation[1][0] += 5.0f;
        break;
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

void keyboardInput(unsigned char key, int x, int y) {
    switch (key) {
    case 'r':
    case 'R':
        printf("Started Rotation!\n");
    default:
        break;
    }

    // Redraw!
    glutPostRedisplay();
}

// Função callback chamada quando o tamanho da janela é alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
    // Evita a divisao por zero
    if (h == 0) h = 1;

    // Especifica as dimens�es da Viewport
    glViewport(0, 0, w, h);

    // Inicializa o sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Estabelece a janela de sele��o (left, right, bottom, top)
    // if (w <= h)
    // 	gluOrtho2D(-2.0f, 2.0f, -2.0f, 2.0f*h / w);
    // else

    // Define "campo de visão"
    //glOrtho(-2.0f, 2.0f, -2.0f, 2.0f, 2.0f, -2.0f);
    gluPerspective(90, w/h, 1, 100);
}

// Programa Principal
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(400, 350);
    glutInitWindowPosition(10, 10);
    glutCreateWindow("Simulado CG");
    glutDisplayFunc(Desenha);
    glutSpecialFunc(rotationInput);
    glutReshapeFunc(AlteraTamanhoJanela);
    Inicializa();
    glutMainLoop();
}
