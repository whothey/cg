CC=g++
LIBS=-lGL -lGLU -lglut -lm
FLAGS=-std=c++11 -Wall
BINDIR=bin

all: quadrado cubo exercicio1 esfera gasket array visualiza cuborot quadricas simulado planet

quadrado: quadrado.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

cubo: cubo.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

exercicio1: exercicio1.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

esfera: esfera.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

gasket: gasket.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

array: array.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

visualiza: visualiza.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

cuborot: cuborot.cpp formas.o
	$(CC) $< "$(BINDIR)/$(word 2, $^)" -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

formas.o: formas.cpp
	$(CC) -c $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

quadricas: quadricas.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

simulado: simulado.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)

planet: planet.cpp
	$(CC) $< -o "$(BINDIR)/$@" $(LIBS) $(FLAGS)
